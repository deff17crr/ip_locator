<?php

namespace ServiceProvider;

use ActiveRecord\Config;
use Pimple\Container;
use Pimple\ServiceProviderInterface;

class ActiveRecordServiceProvider implements ServiceProviderInterface
{
    /** @var  string */
    private $host;

    /** @var  string */
    private $dbName;

    /** @var  string */
    private $user;

    /** @var  string */
    private $password;

    /**
     * ActiveRecordServiceProvider constructor.
     * @param string $host
     * @param string $dbName
     * @param string $user
     * @param string $password
     */
    public function __construct($host, $dbName, $user, $password)
    {
        $this->host = $host;
        $this->dbName = $dbName;
        $this->user = $user;
        $this->password = $password;
    }

    /**
     * @param Container $pimple
     */
    public function register(Container $pimple)
    {
        Config::initialize(function (Config $cfg) {
            $cfg->set_model_directory(__DIR__.'/../Model/');
            $cfg->set_connections([
                'development' => sprintf('mysql://%s:%s@%s/%s', $this->user, $this->password, $this->host, $this->dbName)
            ]);
            $cfg->set_default_connection('development');
        });
    }
}

<?php
namespace Config;

use Service\IpLocator;
use Silex\Application;

/**
 * Class ServiceContainer
 * @package Config
 */
class ServiceContainer
{
    /**
     * @param Application $app
     */
    public static function apply(Application $app)
    {
        $app['ip.locator'] = function () {
            return new IpLocator();
        };
    }
}

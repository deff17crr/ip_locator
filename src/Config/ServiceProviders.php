<?php
namespace Config;

use ServiceProvider\ActiveRecordServiceProvider;
use Silex\Application;
use Silex\Provider\TwigServiceProvider;

/**
 * Class ServiceProviders
 * @package Config
 */
class ServiceProviders
{
    /**
     * @param Application $app
     */
    public static function apply(Application $app)
    {
        $app->register(new TwigServiceProvider(), array(
            'twig.path' => __DIR__.'/../Resources/views',
        ));

        $app->register(new ActiveRecordServiceProvider('localhost', 'test', 'root', 'root'));
    }
}

<?php
namespace Controller;

use Model\IpLocation;
use Service\IpLocator;
use Silex\Api\ControllerProviderInterface;
use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MainControllerProvider
 * @package Controller
 */
class MainControllerProvider implements ControllerProviderInterface
{
    /**
     * @param Request $request
     * @param Application $app
     * @return mixed
     */
    public function indexAction(Request $request, Application $app)
    {
        $ip = $request->getClientIp();

        return $app['twig']->render('index.html.twig', ['ip' => $ip]);
    }

    /**
     * @param Request $request
     * @param Application $app
     * @return string
     */
    public function getLocationAction(Request $request, Application $app)
    {
        $ip = $request->getClientIp();

        $ipLocator = $app['ip.locator'];
        $ipLocation = $ipLocator->getLocation($ip);
        
        if (null === $ipLocation) {
            return new JsonResponse(['error' => 'Can\'t determine location'], 400);
        }
        
        return new JsonResponse($ipLocation->to_array());
    }

    /**
     * @param Application $app
     * @return mixed
     */
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers
            ->get('/', [$this, 'indexAction'])
            ->bind('homepage');

        $controllers
            ->get('/get-location', [$this, 'getLocationAction'])
            ->bind('get_location');

        return $controllers;
    }
}

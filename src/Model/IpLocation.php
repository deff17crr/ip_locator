<?php
namespace Model;

use ActiveRecord\Model;

/**
 * Class IpLocation
 * @package Model
 */
class IpLocation extends Model
{
    public static $table_name = 'ip_location';
}

<?php
namespace Service;

use Model\IpLocation;

/**
 * Class IpLocator
 * @package Service
 */
class IpLocator
{
    const URL = 'http://ipinfo.io/';

    /**
     * @param string $ip
     * @return IpLocation|null
     */
    public function getLocation($ip)
    {
        $ipLocation = IpLocation::find_by_ip($ip);
        if (null !== $ipLocation) {
            return $ipLocation;
        }

        /* or install guzzle */
        $result = file_get_contents(self::URL . $ip);
        $result = json_decode($result, true);
        if (!array_key_exists('country', $result) && !array_key_exists('region', $result)) {
            return null;
        }

        $country = array_key_exists('country', $result) ? $result['country'] : 'Unknown';
        $region = array_key_exists('region', $result) ? $result['region'] : 'Unknown';

        $ipLocation = new IpLocation();
        $ipLocation->ip = $ip;
        $ipLocation->country = $country;
        $ipLocation->region = $region;
        $ipLocation->save();

        return $ipLocation;
    }
}

(function($) {
    var get_location_button = $('.get-location');
    var location = $('.location');
    var url = get_location_button.attr('href');
    var loader = $('.loader');
    
    get_location_button.on('click', function (e) {
        e.preventDefault();
        loader.removeClass('hidden');
        location.html('');

        $.ajax({
            url: url,
            success: function (data) {
                location.append($('<div/>').text('Country: ' + data.country));
                location.append($('<div/>').text('Region: ' + data.region));
            },
            complete: function (response) {
                if (!response.hasOwnProperty('responseJSON')) {
                    location.append('<div class="alert alert-danger">Unknown error.</div>');
                }
                else if (response.responseJSON.hasOwnProperty('error')) {
                    location.append($('<div class="alert-danger alert"/>').text(response.responseJSON.error));
                }

                loader.addClass('hidden');
            }
        });
    });
})(jQuery);
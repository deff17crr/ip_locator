<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$loader = require_once __DIR__.'/../vendor/autoload.php';
$loader->add(false, __DIR__.'/../src');

use Silex\Application;
use Controller\MainControllerProvider;
use Config\ServiceProviders;
use Config\ServiceContainer;

$app = new Application();
ServiceProviders::apply($app);
ServiceContainer::apply($app);

$app->mount('/', new MainControllerProvider());

$app->run();
